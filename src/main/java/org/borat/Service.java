package org.borat;

import io.grpc.stub.StreamObserver;

import java.util.logging.Logger;

public class Service extends RKeeperGrpc.RKeeperImplBase {
  private Logger logger = Logger.getLogger(this.getClass().getName());

  @Override
  public void addOrder(AddOrder request, StreamObserver<AddOrder.Response> responseObserver) {
    logger.info("received a request");
    // RKeeper similar service integration expected
    responseObserver.onNext(AddOrder.Response.newBuilder().setStatus(true).build());
    responseObserver.onCompleted();
  }
}
