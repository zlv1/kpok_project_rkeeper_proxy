package org.borat;

import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;

import java.io.IOException;
import java.util.logging.Logger;

public class Application {

  public static void main(String[] args) throws IOException, InterruptedException {
    ServerBuilder<?> serverBuilder = ServerBuilder.forPort(50052);
    serverBuilder.addService(new Service());
    io.grpc.Server server = serverBuilder.build();
    server.start();
    server.awaitTermination();
  }
}
